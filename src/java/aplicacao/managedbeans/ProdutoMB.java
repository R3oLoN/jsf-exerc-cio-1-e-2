/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacao.managedbeans;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author comp2
 */
@Named(value = "produtoMB")
@ViewScoped
public class ProdutoMB {

    private String description;
    private Double price;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void salvar(){
        System.out.println("Dewscrição: "+ description);        
        System.out.println("Preço: "+ price);        
    }
    
    /**
     * Creates a new instance of ProdutoMB
     */
    public ProdutoMB() {
    }

}
